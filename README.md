# Jubelio ecommerce

We bring you a mobile app for easier buying anything you want.

# Getting Started
    To running this app in local
      - Clone this repo with git clone https://gitlab.com/MasselYulia/project-test.git
      - npm install to install all reqruired dependencies
      - Create a .env  file 
      - node index.js or npm start to start the local server

  

# Equipment
    This project using with :

      - React JS (Framework)
      - Material UI react 
      - Redux

# Feature
- **Home**, you can see all product.
- **Cart**, You can see and mange your product choice.
- **Detail Product**, You can read anything for detail product like description etc.


# Snippets

- Home

![Home](/public/assets/img/readme/homePage.png)
  
- Detail Product

![Detail Product](/public/assets/img/readme/detail-product.png)

- Cart

![Cart](/public/assets/img/readme/Cart.png)



  

## Library Use

1. MUI offers a comprehensive suite of UI tools to help you ship new features faster. Start with Material UI, our fully-loaded component library, or bring your own design system to our production-ready components.

2. React (also known as React.js or ReactJS) is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta (formerly Facebook) and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.

import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./Pages/Home/Home";
import DetailProduct from "./Pages/DetailProduct/index";
import Cart from "./Pages/Cart/Cart";

// import Cart from "./components/Cart";

const App = () => {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/detail-product" element={<DetailProduct/>}/>
      <Route path="/Cart" element={<Cart/>}/>
    </Routes>
    </BrowserRouter>
  );
}

export default App;





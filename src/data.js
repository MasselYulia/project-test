export const listItem = [
    {
      id: "2",
      name: "Album",
      image: "assets/img/album.jpg",
      price: 100000,
      stock: "2",
      short_description:"This is a simple, virtual product.",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "1",
      name: "Beanie",
      image: "assets/img/beanie.jpg",
      price: 150000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "2",
      name: "Beanie with Logo",
      image: "assets/img/beanie-with-logo.jpg",
      price: 130000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "1",
      name: "Belt",
      image: "assets/img/belt.jpg",
      price: 150999,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "2",
      name: "Brand Buttons",
      image: "assets/img/DE-Pins.jpg",
      price: 80000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "1",
      name: "Cap",
      image: "assets/img/cap.jpg",
      price: 99000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "2",
      name: "Dat Divi Engine Life Crop-top ( 3-Tone )",
      image: "assets/img/Dat-Divi_Life.jpg",
      price: 180000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "1",
      name: "Dat Divi Engine Life Hoodie - Limited Edition",
      image: "assets/img/Hoodie.jpg",
      price: 150000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
      id: "2",
      name: "Divi Engine Logo Zipper Hoodie",
      image: "assets/img/Hoodie-Women.jpg",
      price: 100000,
      stock: "2",
      short_description:"",
      description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
    },
    {
        id: "2",
        name: "Divi Engine String Bag ( Big Logo )",
        image: "assets/img/Bag1.jpg",
        price: 100000,
        stock: "2",
        short_description:"",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
      },
      {
        id: "2",
        name: "Divi Engine String Bag ( Small Logo )",
        image: "assets/img/Bag2.jpg",
        price: 100000,
        stock: "2",
        short_description:"",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
      },
      {
        id: "2",
        name: "Divi Engine Tee",
        image: "assets/img/Shirt-3-yellow.jpg",
        price: 100000,
        stock: "2",
        short_description:"",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius quasi vero ducimus animi similique omnis rem in reprehenderit sequi non deleniti ipsa debitis, facere qui libero, odio placeat! Rerum quaerat distinctio ipsa modi cupiditate nulla architecto id odio similique beatae nobis inventore consequatur eum porro aperiam maxime, dignissimos maiores."
      }
  ];
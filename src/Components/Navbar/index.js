import React from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";

const Navbar = () => {
  return (
    <div className="navbar">
      <div className="logo">
        <img src="/assets/img/logo/logo.png" alt="" width={280}/>
      </div>
      <ul>
        <li>
          <NavLink activeClassName="active" to="/" className="navlink">
            <p className="fw-bold ms-5 mt-2">Home</p>
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/Cart" className="navlink">
            <p className="fw-bold ms-5 mt-2">Cart</p>
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/Checkout" className="navlink">
            <p className="fw-bold ms-5 mt-2">Checkout</p>
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/home" className="navlink">
            <p className="fw-bold ms-5 mt-2">My account</p>
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/home" className="navlink">
            <p className="fw-bold ms-5 mt-2">Sample page</p>
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;

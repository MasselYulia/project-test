import React from 'react';
import { Link } from 'react-router-dom';
import "./footer.css"

const Footer = () => {
  return (
    <footer>
        <p>© Jubelio Wordpress Developer Code Testing 2022</p>
        <Link>Built with Storefront & WooCommerce.</Link>
    </footer>
  )
}

export default Footer
import React, { useState } from "react";
import { Grid } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { listItem } from "../../data";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import IconButton from "@mui/material/IconButton";
import Navbar from "../../Components/Navbar/index";
import "./detailproduct.css";
import Button from "../../Components/Button";
import Footer from "../../Components/Footer";

const DetailProduct = () => {

  const [activeList, setActiveList] = useState("firstList");
  const navigate = useNavigate();
  const cart = useSelector((state) => state);
  console.log(cart);
  const dispatch = useDispatch();
  const addition = (acc, currentvalue) => {
    return acc + currentvalue.price * currentvalue.quantity;
  };
  const total = cart.reduce(addition, 0);
  console.log("INI TOTAL", total);
  // const handleClick = () => {
  //   navigate("/succes");
  // };
  const [category, setCategory] = useState("");

  const handleChange = (event) => {
    setCategory(event.target.value);
  };

  const handleClick = () => {
    navigate("/detail-product")
  };

  return (
    <Grid container spacing={0}>
      <Navbar />
      <div className="menu-search">
        <div className="sorting">
          <FormControl className="form-control" sx={{ m: 1, minWidth: 150 }}>
            <Select
              value={category}
              onChange={handleChange}
              displayEmpty
              inputProps={{ "aria-label": "Without label" }}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
          <input
            className="input-search"
            type="search"
            placeholder="search here..."
            name=""
            id=""
          />
        </div>
        <div className="info-cart">
          {total > 0 && <p>Rp. {total} </p>}
          <span>0 items</span>
          <IconButton color="primary" aria-label="cart">
            <ShoppingCartIcon />
          </IconButton>
        </div>
      </div>
      <div className="detail-product">
        {cart.map((item) => {
          return (
            <div>
              <div className="detail-item" key={item.id}>
                <img src={item.image} alt="" width={500} />
                <div className="short-desc">
                  <p>{item.name}</p>
                  <h6>Rp. {item.price}</h6>
                  <p className="desc">{item.short_description}</p>
                  <div className="quantity">
                    <button
                      className="btn-temp"
                      onClick={() => {
                        if (item.quantity > 1) {
                          dispatch({ type: "DECREEMENT", payload: item });
                        } else {
                          dispatch({ type: "REMOVE", payload: item });
                        }
                      }}
                    >
                      -
                    </button>
                    <div className="qt">{item.quantity}</div>
                    <button
                      className="btn-temp"
                      onClick={() =>
                        dispatch({ type: "INCREEMENT", payload: item })
                      }
                    >
                      +
                    </button>
                    <div>
                      <Button className="btn-add">Add To Cart</Button>
                    </div>
                  </div>
                  <hr />
                  <p>SKU: woo-album</p>
                  <p>Category: Music</p>
                </div>
              </div>
              <div className="content-desc">
              <div className="list-group">
            <a
              onClick={() => setActiveList("firstList")}
              href="#"
              className="list-group-item list-group-item-action pt-3"
              aria-current="true"
            >
              description
            </a>
            <a
              onClick={() => setActiveList("secondList")}
              href="#"
              className="list-group-item list-group-item-action pt-3"
              aria-current="true"
            >review</a>
            </div>
              {activeList === "firstList" && (
                <div className="description">
                  <p>Description</p>
                  {item.description}
                </div>
          )}
          {activeList === "secondList" && (
             <div className="form">
              <form action="">
                <input type="text" />
              </form>
             </div>
          )}
              </div>
              <div className="related">
                <p className="title">Related products</p>
                <div className="related-product">
              {listItem.map((listProducts, idx) => {
          listProducts.quantity = 1;
          return (
            <div className="item">
              <div className="img" onClick={()=>dispatch({type: "ADD",payload: listProducts},handleClick())}>
                <img src={listProducts.image} alt="" />
              </div>
              <div className="item-bot">
                <p>{listProducts.name}</p>
                <h6>Rp. {listProducts.price}</h6>
                <Button
                  className="btn-add"
                  onClick={() =>
                    dispatch(
                      { type: "ADD", payload: listProducts },
                      handleClick()
                    )
                  }
                >
                  Add To Cart
                </Button>
              </div>
            </div>
          );
        })}
              </div>
              </div>
            </div>
          );
        })}
      </div>
      <Footer/>
    </Grid>
  );
};

export default DetailProduct;

import { Grid } from "@mui/material";
import React, { useState } from "react";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import ClearIcon from "@mui/icons-material/Clear";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Navbar from "../../Components/Navbar";
import "./Cart.css";
import { styled } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Button from "../../Components/Button";
import Footer from "../../Components/Footer";

const Cart = () => {
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.info.light,
      color: theme.palette.common.white,
      //   borderRadius:15px,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  const navigate = useNavigate();
  const cart = useSelector((state) => state);
  console.log(cart);
  const dispatch = useDispatch();
  const addition = (acc, currentvalue) => {
    return acc + currentvalue.price * currentvalue.quantity;
  };
  const total = cart.reduce(addition, 0);
  console.log("INI TOTAL", total);
  // const handleClick = () => {
  //   navigate("/succes");
  // };

  return (
    <Grid container spacing={0}>
      <Navbar />
      <div className="menu-search">
        <div className="sorting">
        <select name="default sorting" id="">
            <option value="">Default sorting</option>
            <option value="">Sort by popularity</option>
            <option value="">Sort by average rating</option>
            <option value="">Sort by lastest</option>
            <option value="">Sort by price: low to high</option>
            <option value="">Sort by price: high to low</option>
          </select>
          <input
            className="input-search"
            type="search"
            placeholder="search here..."
            name=""
            id=""
          />
          <SearchOutlinedIcon className="icon-search"/>
        </div>
        <div className="info-cart">
          {total > 0 && <p>Rp. {total} </p>}
          <span>0 items</span>
          <IconButton color="primary" aria-label="cart">
            <ShoppingCartIcon />
          </IconButton>
        </div>
      </div>
      <div className="content-cart">
        <TableContainer sx={{ borderRadius: "8px" }}>
          <Table sx={{ minWidth: 1310 }} aria-label="simple table">
            <TableHead>
              <StyledTableRow>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell align="right">Product</StyledTableCell>
                <StyledTableCell align="right">Price</StyledTableCell>
                <StyledTableCell align="center">Quantity</StyledTableCell>
                <StyledTableCell align="right">Subtotal</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {cart.map((item) => (
                <StyledTableRow
                  key={item.id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <StyledTableCell>
                    {" "}
                    <Button
                      onClick={() =>
                        dispatch({ type: "REMOVE", payload: item })
                      }
                      className="btn-del"
                    >
                      <ClearIcon />
                    </Button>
                  </StyledTableCell>
                  <StyledTableCell component="th" scope="row">
                    <img src={item.image} alt="" width={100} />
                  </StyledTableCell>
                  <StyledTableCell align="right">{item.name}</StyledTableCell>
                  <StyledTableCell align="right">{item.price}</StyledTableCell>
                  <StyledTableCell align="center">
                    {" "}
                    <div className="quantity">
                      <button
                        className="btn-temp"
                        onClick={() => {
                          if (item.quantity > 1) {
                            dispatch({ type: "DECREEMENT", payload: item });
                          } else {
                            dispatch({ type: "REMOVE", payload: item });
                          }
                        }}
                      >
                        -
                      </button>
                      <div className="qt">{item.quantity}</div>
                      <button
                        className="btn-temp"
                        onClick={() =>
                          dispatch({ type: "INCREEMENT", payload: item })
                        }
                      >
                        +
                      </button>
                    </div>
                  </StyledTableCell>
                  <StyledTableCell align="right">{total > 0 && <p>Rp. {total} </p>}</StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <div className="menu-sale">
            <div>
            <input type="text" value="" placeholder="Coupon code" />
            <Button>Apply coupon</Button>
            </div>
            <div>
                <Button>Update cart</Button>
            </div>
        </div>
        <div className="cart-total">
            <p className="txt-total">Cart Totals</p>
            {cart.map((item) => (
            <div className="totals" key={item.id}>
                <div className="list-left">
                    <div className="name-list">Subtotal</div>
                    <div className="desc-list">{item.price}</div>
                </div>
                <div className="list-left">
                    <div className="name-list">Shipping</div>
                    <div className="desc-list">	
Free shipping
Shipping to DKI Jakarta.</div>
                </div>
                <div className="list-left">
                    <div className="name-list">Total</div>
                    <div className="desc-list">{total > 0 && <p>Rp. {total}</p>}</div>
                </div>
            </div>
                ))}
                <Button className="btn-procced">Procced to checkout <ArrowRightAltIcon fontSize="large"/></Button>
        </div>
      </div>
      <Footer/>
    </Grid>
  );
};

export default Cart;

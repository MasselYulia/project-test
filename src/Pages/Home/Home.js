import { Grid } from "@mui/material";
import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { listItem } from "../../data";
import { useSelector, useDispatch } from "react-redux";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import Pagination  from '@mui/material/Pagination';
import usePagination from "../../Components/Pagination/Pagination";
import { default as data } from "../../Pagination.json";
import IconButton from "@mui/material/IconButton";
import Navbar from "../../Components/Navbar";
import Button from "../../Components/Button/index";
import "./home.css";
import Footer from "../../Components/Footer";

const Home = () => {
  const [search, setSearch] = useState("");
  const [cart] = useSelector((state) => state);
  console.log(cart);
  const dispatch = useDispatch();
  const navigate = useNavigate()

  const handleClick = () => {
    navigate("/detail-product")
  };
  const handleAddCart = () => {
    navigate("/Cart")
  };

  let [page, setPage] = useState(1);
  const PER_PAGE = 12;

  const count = Math.ceil(data.length / PER_PAGE);
  const _DATA = usePagination(data, PER_PAGE);

  const handleChangePage = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };

  return (
    <Grid container spacing={0}>
      <Navbar />
      <div className="menu-search">
        <div className="sorting">
          <select name="default sorting" id="">
            <option value="">Default sorting</option>
            <option value="">Sort by popularity</option>
            <option value="">Sort by average rating</option>
            <option value="">Sort by lastest</option>
            <option value="">Sort by price: low to high</option>
            <option value="">Sort by price: high to low</option>
          </select>
          <input
            className="input-search"
            type="search"
            placeholder="search here..."
            name=""
            id=""
            onChange={(e)=>{setSearch(e.target.value)}}
          />
          <SearchOutlinedIcon className="icon-search"/>
        </div>
        <div className="info-cart">
          <p>Rp 0</p>
          <span>0 items</span>
          <IconButton color="primary" aria-label="cart">
            <ShoppingCartIcon />
          </IconButton>
        </div>
      </div>
      <div className="pagination">
      <Pagination
        count={count}
        size="large"
        page={page}
        variant="outlined"
        shape="rounded"
        onChange={handleChangePage}
      />
      </div>
      <div className="products">
      {_DATA.currentData().filter((listProducts) => {
          // eslint-disable-next-line eqeqeq
          if (search == "") {
            return listProducts
          }else if(listProducts.name.toLowerCase().includes(search.toLowerCase())){
            return listProducts
          }
        }).map((listProducts, idx) => {
          listProducts.quantity = 1;
          return (
            <div className="item">
              <div className="img" onClick={()=>dispatch({type: "ADD",payload: listProducts},handleClick())}>
                <img src={listProducts.image} alt="" />
              </div>
              <div className="item-bot">
                <p>{listProducts.name}</p>
                <h6>Rp. {listProducts.price}</h6>
                <Button
                  // className="btn-add"
                  onClick={() =>
                    dispatch(
                      { type: "ADD", payload: listProducts },
                      handleAddCart()
                    )
                  }
                >
                  Add To Cart
                </Button>
              </div>
            </div>
          );
        })}
      </div>
      <div className="pagination">
      <Pagination
        count={count}
        size="large"
        page={page}
        variant="outlined"
        shape="rounded"
        onChange={handleChangePage}
      />
      </div>
      <Footer/>
    </Grid>
  );
};

export default Home;
